Jathan
18.03.2020 - Add Paul Sutton as admin in Moodle.
15.09.2020 - Add Mechtilde Stehmann as course creator in System Roles.
15.09.2020 - Add Paul Sutton as course creator in System Roles.
15.09.2020 - Add Emmanuel Arias as course creator in System Roles.
15.09.2020 - Add Jonathan Carter as course creator in System Roles.
15.09.2020 - Installation of Deutsch, Español - internacional (es) and Portugues - Brasil (pt_br) language packages. 
