**Steps to Develop a Concept for the Debian Academy**

A proposed model for a structured planning process.

-----

# PATOMCIE

## 1. PERSONS

Who is involved, as providers (organizers, trainers) and participants?

What do we know about them, their explicit or guessed needs and
expectations, their prior and/or required knowledge, skills, attitudes?

~~~
Examples:

* Advanced users, developers, admins, artists … who are interested to learn
  how Debian is "made" and expect information and "recipes" to make
  contributions.
* Debian members with experience in mentoring/teaching.
* …
~~~

## 2. AIM

What is the overall aim of the Debian Academy, the grand vision, the
lighthouse we're sailing towards?

~~~
Example:

The Debian Academy is an e-learning platform for learning about how to
contribute to the Debian project.
~~~

## 3. TOPIC AREAS

What are the contents, the topics, the subjects that are covered by the
Debian Academy?

~~~
Examples:

* Debian Packaging
* Reproducible Builds
* Translations
* Events/Video Team
* Documentation/Website
* Publicity
* …
* (more fine-grained topics)
~~~

## 4. OBJECTIVES

Which objectives does the Debian Academy want to achieve for each topic,
i.e. what will the participants have learned? ‘Objectives’ refers to skills,
knowledge and attitudes with each objective having one simple measurable
outcome. Objectives should be SMART. [0]

~~~
Examples:

* Topic packaging:
	- Participants know what a source and a binary package is.
	- Participants have re-built a binary package from source themselves.
	- Participants understand that debian/copyright files are tedious but
	  essential for DFSG reasons
	- (and _many_ more)
~~~

## 5. METHODS

### 5.a The big picture

How are these objectives achieved? What structures, tools, processes,
activities, resources can be used to achieve the objectives for each topic?

~~~
Examples:

* Documentation (written)
* Stand-alone video tutorials
* Interactive webinars, tutoring
* Courses (multi-part trainings)
* …
~~~

### 5.b Detailed planning

How can we put this all together? Which curriculum do we need? Which
materials need to be produced? Which people need to be involved? How does
communication work? Which infrastructure and software needs to be set up?

~~~
Examples: (here goes the real work :))

* Topic packaging:
	- Make existing documentation easily available (https://www.debian.org/doc/ could use some love)
	- Create videos following Lucas' packaging tutorial (Debian package "packaging-tutorial") with short steps, to
	  view "at home" - video platform (peertube?)
	- Offer webinars to interactively show these packaging steps in practice - 
	  webconference platform (big blue button)
	- Design courses as in a sequence of docs, videos, tasks, webinars - 
	  LMS (moodle)
~~~

## 6. CROSSCHECK

Before starting the implementation: Do all the steps so far fit together,
are they logically consistent, have we forgotten anything?

~~~
Left until we are that far in the process.
~~~

## 7. IMPLEMENTATION

Are we ready? Release it! :)

~~~
Left until we are that far in the process.
~~~

## 8. EVALUATION

Have the objectives been achieved? Were the methods and the x adequate? Has
the implementation worked?

And now: Back to square 1 for the next round!

~~~
Left until we are that far in the process.
~~~

-----

[0] SMART:

- S = Simple: Each objective should contain one idea so that it is easily understood and easy to measure whether it has been achieved or not.
- M = Measurable: To know whether you have achieved what you set out to do, objectives must include a fixed target. There should also be a tangible outcome to each objective.
- A = Achievable: The target set must be achievable by the team or person for which it is being set.
- R = Realistic (e.g. time constraints); or
- R = Relevant: Each objective needs to have relevance for the overall environment, visons, and topics.
- T = Timed: To know whether an objective has been achieved it needs to be set in a time frame. Otherwise the target might remain possible but never achieved.

-----

Source 1: Spirale

1. Personen und Standortbestimmung
1. Bedürfnisse und Erwartungen
1. Ziel
1. Inhalte
1. Lernziele
1. Methoden
1. Programm
1. Rückblick
1. Durchführung
1. Auswertung

-----

Source 2: NAOMIE

(Needs, Aims, Objectives, Methods, Implementation,Evaluation)

<https://members.scouts.org.uk/documents/supportandresources/Trainers_resources/Module%2031%20.pdf>

N: Specific NEED identified  
Difference between needs and wants, the sources of different needs.

A: General AIM decided (outline what it is you wish to achieve)  
An aim is a statement of what you wish to achieve.

O: Detailed OBJECTIVES set (specify what is to be done by theother person/s)  
Objectives describe what the participants will achieve, written in terms of measurable outcomes within a timescale. ‘Objectives’ refers to skills, knowledge and attitudes with each objective having one simple measurable outcome. Objectives should be SMART. [0]

M: METHOD (describe how you are going to meetthe objectives)  
Methods are the ways in which something is done to achieve the objective. There are usually many alternatives.

I: IMPLEMENT (follow the method you detailed)  
Implementation is the process of putting the plan into effect.

E: EVALUATE the success of the activity  
Evaluation is the process of measuring the result of the plan against theobjectives set.


----

Some interesting methods for course development:

* ABC Learning Design <https://abc-ld.org/>
* Backward Course Design <https://wit.edu/lit/develop/backward-course-design>
