URL of this pad: https://storm.debian.net/shared/jvdrY9rsw8SIF9FcgDtHxZsBWZHGJWh2xLIZqa3Wj1j
    
Debian Academy Related things

Google Season of Docs 
we could get help from technical writers to transform some Debian documentation into a Moodle or EdX course
https://wiki.debian.org/SeasonOfDocs2019

Thread "academic alliances or the like" in -project in 2016
https://lists.debian.org/debian-project/2016/04/msg00002.html

Other links
https://wiki.debian.org/Courses/MaintainingPackages
https://wiki.debian.org/Courses2005/BuildingWithoutHelper
https://wiki.debian.org/DebianWomen/Projects/Events/TrainingSessions
https://wiki.debian.org/Moodle
https://campus.openhatch.org/


PARTNERSHIP WITH TEACHING ORGS/COMPANIES

Some companies contacted Debian about partnership related to teaching courses with Debian or about Debian.
My listing (recent to older)
    1.
   High School Technology Services, Washington DC USA - contacted in 2019. We couldn't offer such kind of partnership, so we suggested to be added as Debian users:
    https://www.debian.org/users/org/myhsts.html
    
    2. Prozcenter
    Mail to partners@:
I own a small training center in Argentina and I want to know if Debian Foundation have would consider partnership with my company (and other Training Centers of course).
The goal of this, from my perspective, is that I belive in education as a key to reach more people, empowering them with te right knowledge and tools (of Debian software in this case).
So my idea is to teach Debian courses, and also generate a fair revenue for the Debian Foundation, Trainig Centers and Trainers. Maybe like Cisco Academy, but with the focus in teach and empower people rather than just sell.

We didn't answer :/

3.
Thread "academic alliances or the like" in -project in 2016
https://lists.debian.org/debian-project/2016/04/msg00002.html

Brainstorm

Target groups and concepts

Gregor Herrmann: "Somehow advanced/experienced users who are curious how this is all made / who want to look behind the curtain and probably find out if and how they could be part of the game themselves."


Communication with Teckids to have a Moodle instance with them for Debian Academy
https://www.teckids.org
https://schul-frei.org/en/2020-03-15_corona.html

Email sent it by Jathan and responded by Teckid staff on Tuesday 01.09.2020

############################################################################
Hi Jonathan,

Am Dienstag, 1. September 2020, 18:36:58, schrieb Jathan:

My name is Jonathan Bustillos and I am a Debian Developer. During our
DebConf 20 last week we have started an E-Learning project called Debian
Academy:

https://wiki.debian.org/DebianAcademy

We would like to ask you if we could use some of your instances to start
creating Debian Development courses officially at your platform. It is
fine if we use Schul Frei to start Debian Academy with your kind support? :)

cool!

I have a few questions:

1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
2. Do you target children who want to learn how to use and contribute to Debian as well?
3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?

I very much consider all of these non-issues, so the next question is: What exactly do you want to use? Your Wiki page is talking about running your own instances of Moodle or the like. Do you want us to do that for you, or do you want to use our public instances? If so, which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?

We are very happy to collaborate with you; in fact, if time had allowed, we would long have started such a project ourselves.

Happy to hear back,
###########################################################################

What do you think about the questions of Teckids?
Jathan answers:1

1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
A: Yes of course :)

2. Do you target children who want to learn how to use and contribute to Debian as well?
A: I have not thought about this before and I am not sure what to say about this to you. It would be wonderful to have children creating courses for Debian, but currently I am not sure if from our part will be possible right now to have courses available for the level of children about Debian development, administration or usage. Do you have any idea to make it possible?

3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?
A: I would say yes, meanwhile those adolescents contributions will be focused on Debian Development or even Debian usage or administration.

4. What exactly do you want to use?
A: A Moodle instance full administrated by the Debian Academy Team members.

5. Do you want us to do that for you, or do you want to use our public instances? 
A: It would be nice to have help from you to do it. Also we have listed at our wiki some interested friends to help in the infrastructure of Debian Academy so we can help too. It would be great to have a Debian Academy instance provided by you with the possibility to be customized by us in everything, having the name Debian Academy instead of Teckids in the main page, to could administrate the courses, the backend, the frontend and to do not have the necessity to send an Email to root@teckids.org to could create a new course.

6. Which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?
A: A Moodle platform will be enough.

I do have access to big blue button if needed. 
    
What do you think everybody? Which would be your answers? Please share them here:

Emmanuel:
1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
A: Yes!!
2. Do you target children who want to learn how to use and contribute to Debian as well?
A: I think that could be a possibility, isn't? I'm really don't have experience making course for children, but maybe into Debian we could found with more experience. The Team https://wiki.debian.org/Kids can be interested on Debian Academy?
3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?
A: I think yes, If I'm not wrong many hackers start from adolescents.
4. What exactly do you want to use?
A: I agree with Jathan. The possibility to create courses, exams, upload videos, attach resources. 
5. Do you want us to do that for you, or do you want to use our public instances? 
A: Agree with Jathan
6. Which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?
A: Moodle. Also can we things about Mattermost? for a direct communication with students? Thinking in a sync class?
Jathan: Interesting about Mattermost. In Moodle you can not have communication between instructors and students?
Natureshadow: Yes you can, but it's an awkward very web-2.0-ish thing. Better to use Moodle for what it's good at (KISS!), and add other things as resources in courses. BigBlueButton actually integrates quite well. I would very much like to look into adding Matrix, because that gives you a well-rounded web thing for beginners, and you get adding IRC rooms from Debian for free on top.
 
tuoermin:
1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
A: Yes.
2. Do you target children who want to learn how to use and contribute to Debian as well?
A: Haven't thought about children, but I would be interested in creating material for kids, as I'll soon have to go through this with my own child.
3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?
A: Yes.
4. What exactly do you want to use?
A:  To begin with Moodle would be more than enough.
5. Do you want us to do that for you, or do you want to use our public instances? 
A: I work as Moodle and Debian admin, so both options are fine for me.
6. Which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?
A: I think own Moodle instance would be best. We might use BBB later, depending on instructor. I'm very interested in possibilities of Matrix/Element and Moodle integration. 

Moozer answers:
1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
A: For teaching material related to debian it would wrong not to use an open license. Licences for documentation would normally use some sort of creative commons
2. Do you target children who want to learn how to use and contribute to Debian as well?
A: Mostly no, since I think that would be very difficult. 
3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?
A: It depends on the quality of the material. I think we want some sort of homogeneous style to the materials, so it will depend a lot on what "contribution" is. Main audience are adults, but older children will often be able to have angles and insights that "adults" don't see.
4. What exactly do you want to use?
A: Moodle - videos, text, quizzes. The way we discuss it now, we also want to track progress for users.
5. Do you want us to do that for you, or do you want to use our public instances? 
A: (no opion currently)
6. Which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?
A: For courses like these, it will mainly be asynchronous, ie. the material is available but we are not "teaching" at specific times. We will need some sort of chat channel for support and QA sessions, but most of it would probably self-paced.
On the other hand - perhaps we could create some sort of buzz and community if we did the courses at a specific time. E.g. 3 weeks twice per year to get people started, get to know then and make sure they get in touch with relevant debian people.
    
Someone answers:
1. Will all of your work be entirely licenced under OSI approved licences (given you are a DD and know your way around FOSS, I suppose yes)
A:
2. Do you target children who want to learn how to use and contribute to Debian as well?
A: 
3. If yes, or "not in the first place", will you accept contributions from adolescents to your material?
A: 
4. What exactly do you want to use?
A: 
5. Do you want us to do that for you, or do you want to use our public instances? 
A: 
6. Which platforms do you want to use (EduGit (GitLab, Mattermost?), Moodle on lms.teckids.org, BigBlueButton on bbb.teckids.org, Matrix/Element on matrix.teckids.org, …?
A:

########## PLANNING MODEL START ##########

Steps to Develop a Concept for the Debian Academy

A proposed model for a structured planning process.

PATOMCIE

1. PERSONS
Who is involved, as providers (organizers, trainers) and participants?
What do we know about them, their explicit or guessed needs and expectations, their prior and/or required knowledge, skills, attitudes?

Examples:

    Advanced users, developers, admins, artists … who are interested to learn how Debian is "made" and expect information and "recipes" to make contributions.

    Debian members with experience in mentoring/teaching.

    …


2. AIM
What is the overall aim of the Debian Academy, the grand vision, the lighthouse we're sailing towards?

Example:
The Debian Academy is an e-learning platform for learning about how to contribute to the Debian project.

3. TOPIC AREAS
What are the contents, the topics, the subjects that are covered by the Debian Academy?

Examples:

    Debian Packaging

    Reproducible Builds

    Translations

    Events/Video Team

    Documentation/Website

    Publicity

    …

    <more fine-grained topics>


4. OBJECTIVES
Which objectives does the Debian Academy want to achieve for each topic, i.e. what will the participants have learned? ‘Objectives’ refers to skills, knowledge and attitudes with each objective having one simple measurable outcome. Objectives should be SMART. [0]

Examples:

    Topic packaging:

    Participants know what a source and a binary package is.

    Participants have re-built a binary package from source themselves.

    Participants understand that debian/copyright files are tedious but essential for DFSG reasons

    <and _many_ more>


5. METHODS
5.a The big picture
How are these objectives achieved? What structures, tools, processes, activities, resources can be used to achieve the objectives for each topic?

Examples:

    Documentation (written)

    Stand-alone video tutorials

    Interactive webinars, tutoring

    Courses (multi-part trainings)

    …


5.b Detailed planning
How can we put this all together? Which curriculum do we need? Which materials need to be produced? Which people need to be involved? How does communication work? Which infrastructure and software needs to be set up?

Examples: (here goes the real work :))

    Topic packaging:

    Make existing documentation easily available

    Create videos following Lucas' packaging tutorial with short steps, to view "at home" - video platform (peertube?)

    Offer webinars to interactively show these packaging steps in practice - webconference platform (big blue button)

    Design courses as in a sequence of docs, videos, tasks, webinars - LMS (moodle)


Some interesting methods for course development:

    ABC Learning Design https://abc-ld.org/

    Backward Course Design https://wit.edu/lit/develop/backward-course-design


6. CROSSCHECK
Before starting the implementation: Do all the steps so far fit together, are they logically consistent, have we forgotten anything?

Left until we are that far in the process.

7. IMPLEMENTATION
Are we ready? Release it! :)

Left until we are that far in the process.

8. EVALUATION
Have the objectives been achieved? Were the methods and the x adequate? Has the implementation worked?
And now: Back to square 1 for the next round!

Left until we are that far in the process.

-----

[0] SMART:

S = Simple: Each objective should contain one idea so that it is easily understood and easy to measure whether it has been achieved or not.
M = Measurable: To know whether you have achieved what you set out to do, objectives must include a fixed target. There should also be a tangible outcome to each objective.
A = Achievable: The target set must be achievable by the team or person for which it is being set.
R = Realistic (e.g. time constraints)
or
R = Relevant: Each objective needs to have relevance for the overall environment, visons, and topics.
T = Timed: To know whether an objective has been achieved it needs to be set in a time frame. Otherwise the target might remain possible but never achieved.

-----

Source 1:
Spirale

    Personen und Standortbestimmung

    Bedürfnisse und Erwartungen

    Ziel

    Inhalte

    Lernziele

    Methoden

    Programm

    Rückblick

    Durchführung

    Auswertung


-----

Source 2:
NAOMIE
(Needs, Aims, Objectives, Methods, Implementation,Evaluation)
https://members.scouts.org.uk/documents/supportandresources/Trainers_resources/Module%2031%20.pdf

N: Specific NEED identified
Difference between needs and wants, the sources of different needs.

A: General AIM decided (outline what it is you wish to achieve)
An aim is a statement of what you wish to achieve.

O: Detailed OBJECTIVES set (specify what is to be done by theother person/s)
Objectives describe what the participants will achieve, written in terms of measurable outcomes within a timescale. ‘Objectives’ refers to skills, knowledge and attitudes with each objective having one simple measurable outcome. Objectives should be SMART. [0]

M: METHOD (describe how you are going to meetthe objectives)
Methods are the ways in which something is done to achieve the objective. There are usually many alternatives.

I: IMPLEMENT (follow the method you detailed)
Implementation is the process of putting the plan into effect.

E: EVALUATE the success of the activity
Evaluation is the process of measuring the result of the plan against theobjectives set.

########## PLANNING MODEL END  ##########

Proposals of concepts for the Debian Academy

Based in the planning models shared by Gregor, I suggest to write our perspective around this important topic.

Jathan
PATOMCIE Model

1. PERSONS
Who is involved, as providers (organisers, trainers) and participants?
A: Teckids and Dominik George are participating as providers of the infrastructure as well as participants and organisers, Debian Developers, contributors, users, Moodle experts, system administrators, programmers, users and people interested to push E-Learning about Debian Development are organisers, currently we do not have trainers and the participants around Debian Academy to take a course should be Debian users. We do want to have participants who would like to be trainers or instructors to create courses and to teach, due they will be the core to share the knowledge about Debian Development to Debian users who are interested to learn how Debian is made and how they could help in different areas inside the Debian Project.

What do we know about them, their explicit or guessed needs and expectations, their prior and/or required knowledge, skills, attitudes?
A: We know the current participants are people committed with Debian, Free Software and with interest to spread knowledge about the Debian Project in an audiovisual way.

2. AIM
What is the overall aim of the Debian Academy, the grand vision, the lighthouse we're sailing towards?
A: The overall aim of Debian Academy is to have an E-Learning platform to teach courses in an easier way about Debian Development to Debian users and contribute to get them involved in Debian teams and activities in a social or in a technical way inside the Debian Project as collaborators, maintainers or developers.

3. TOPIC AREAS
Debian tools used for communication in the community (IRC, Mailing Lists), Debian Packaging, Reproducible Builds, Translations, Video Team, Salsa GitLab, Web, Publicity, Debian Live, Infrastructure and any Debian Development topic related.

4. OBJECTIVES
Which objectives does the Debian Academy want to achieve for each topic, i.e. what will the participants have learned? ‘Objectives’ refers to skills, knowledge and attitudes with each objective having one simple measurable outcome. Objectives should be SMART.
A: 

    Topic Debian tools used for communication in the community:

    Participants know what IRC and a mailing list are.

    Participants know to install and configure an IRC client.

    Participants know how to subscribe and participate in the Debian mailing lists.

    Participants identify the Debian IRC channels and the mailing lists regarding to their interests.


    Topic Debian Packaging:

    Participants know the importance and the function of a Debian package.

    Participants know what a source and a binary package is.



Requesting instructor account in the Debian Academy Moodle instance

    Instructor creates account in Moodle (either with Salsa or by e-mail registration).

    Files a course request that one of the administrators then confirms. If needed an instructor can also get a 'course creator' role - can create course by himself.


----- COPY OF THE ETHERPAD IN https://pad.online.debconf.org/p/30-debian-academy-another-way-to-share-knowlegde

Debian Academy: Another way to share knowledge about Debian
    
/*
History: there were tutorials on IRC, at #debian-meeting, organized by Debian Women
https://wiki.debian.org/DebianWomen/Projects/Events/TrainingSessions
*/
A: Thanks for pointing this.

Q:  Is Valeria a real person? (-:
A: Let's see :) No, she is not :D

Additionally: webinars on (jitsi or) bigbluebutton
A: Yes, those kind of contents and tools could work too.

Q. Do you have a set of topics in mind?
A: Initially I have thought about Debian Packaging, Debian tools used for communication in the community (IRC, Mailing Lists) translations (l10n), Reproducible Builds and Debian Salsa, just my ideas, but it should be a project for every topic as possible from the technical to the social thing.


Comment: there's some videos about package management (not packaging) at https://highvoltage.tv/videos/watch/playlist/7bac4666-6ab7-4acb-92ee-70dac2e4a014
A: Thanks. Yes, even some of these videos were kind of inspiration to think about the Debian Academy project.

Q:  Jathan, What do you believe are first steps to creating such an acadamy?  What hurdles to jump?   Ideas of how to organize trainers?  What role would you like to have? +1
A:  Use inspiration from tech used by Debian Social (federated web etc) but focused on education with a centralized Debian Acadamy platform.
A:  Jathan would like a social communications role for sharing and building awareness of this idea.
A: I think the first steps should be get together all the people who find this purpose relevant, create a team named "Debian Academy", to create a Mailing List, IRC channel, to organise the brainstorming and start the planning. The hurdles to jump currently are just to start :) To have ideas about how to organise the trainers, first we need to have people who desire to be trainers and then see what to define. Regarding to the role, I would like to help with the infrastructure of Debian Academy and with the communication.

Q: Would you put your email up again?
jathan@d.o

COMMENT: Eriberto (eriberto@d.o) has an entire structured Debian packaging course on http://debianet.com.br/, with SEVERAL hours of video + a , and has been using this for several years to train a lot of people in packaging. I think his experience is something that this could build upon. (pt_BR though)
A: Of course and is he is a wonderful teacher! I met him at DC19 in Curitiba and I agree about his training contents, a complete course but currently available only in Portuguese and hard to find since it is on his personal web site.

COMMENT: e-learning (or a debian academy) needs more than a series of video tutorials, first a concept, a "curriculum", didactic background, target groups …
A:  I agree with this point (Jathan says).  Lots of skills needed here, and need team members to decide.

Q: Are this platform going to be for end-users or just for more advanced users?
A: Any type of level.

COMMENT: If you show prerequsites of knowledge needed to understand the topic, then you could start anywhere up and down the curriculum.

COMMENT: You could build the course content around the objectives of a certification such as LPI's LPIC-1 or Linux Essentials.   Then people following your courses could get a certificate.   That gives you structure and flow that is useful for (perhaps) getting a job.
A: That could be something useful related to Debian and jobs. Certifications are not my main goal for Debian Academy, but if it could be interesting to see what happen if Debian Academy give diplomas or certifications to people interested on them.

Q. You say videos are not structured. Would you consider that making a browseable index of existing material (DebConf or external) categorized by topic and language be an acceptable starting point? +1 +1, curating existing material could be *great*, +1 peertube makes the titles searchable at least
A:  Yes a great way to start.

Comment: seems there is some indexing at https://salsa.debian.org/debconf-video-team/archive-meta pretty minimal, it's just data about the structure, not much on the content (maybe a place to improve?) -- Metadata is the most difficult part. I [alvarezp] tried "digitalizing" metadata in a spreadsheet before finding out we already had archive-meta so it's nice to see that the most difficult part is already done.
A: Thanks for sharing that metadata repo!

Q: Many of the materials that would be created for this academy you propose would be general, about Linux - And there is no shortage of those! Defining a project scope is fundamental for its success. So, where would you draw the line? How "deep" into free software use and development would you call adequate for the Debian Academy, and how much would you leave fo others? 
A: Absolutely. The scope will be focused only to Debian and specifics contents around its different fields, not in Free Software and GNU/Linux in general. I would draw the line based on the current activities and existing teams of the Debian Project.


COMMENT:  One way to determine scope of trainings is to discover motivations of a first set of contributors, but contributors need to know scope to decide if they should attempt contributions. . . So a chicken and egg problem.
A: Not really a chicken and egg problem. The scopes should be focused initially on the main topics that are hard to understand for someone new interested about helping directly to Debian.

----- END OF COPY OF THE DEBCONF ETHERPAD

A strategy for evaluating Debian Academy courses

My notes outline a brief overview of what peer reviewing is and what it entails. I also include an example of a computing resource which uses peer review techniques for publishing lessons. Finally, I end my notes by making a case for Debian Academy to adopt similar methods.

# Peer review 
Peer review is the evaluation of work by others who are working in the same field. Peer review is often used to help make the work well-balanced and high quality. If you are submitting material to be peer reviewed you will typically receive notes which critiques your work. If results are found to be wrong then the author will usually correct their mistake and resubmit. The work passes peer review when a consensus is formed.

To ensure objectivity and create work that can be trusted, the peer review process is often the answer. The process of reviewing papers is fundamental in academic publications. Universities, for example, typically involve an array of authorities when "accepting" papers. This can consist of departmental figures as well as external reviewers such as journals, who usually employ an international body of scientists for any given field.

Scholars shall not be surprised by peer review, however, computing blogs and online courses are usually not subjected to the same level of thorough evaluation. Computing blogs tend to write informally and come from an independent voice, a trend that tends to make them immensely popular. Both corroborated and independent styles should be allowed to co-exist, since both can be effective at expressing ideas. The case for adopting a peer review approach, however, will be to guarantee the instantiation of a trustworthy resource by enforcing standards.  We often recognise W3C and Mozilla for their approach to soliciting wide reviews. It could be argued that Wikipedia, a peer reviewed site, has had issues regarding public perception, especially when people learnt that pages could be edited by experts and amateurs alike. Subsequently, Wiki-
pedia has garnered huge reach and is many people's first choice when conducting preliminary research.         

## The Programming Historian
https://programminghistorian.org/ 

The Programming Historian (PH) is a freely available, CC licensed, resource dedicated to teaching computing to a wide range of readers. Personally speaking, they were the first website that I experienced asking readers to critique their work. They have also developed a useful platform to assist reviewers. The website which homes The Programming Historian always advertises ways for the reader to contribute. They have written guides to help instruct people in doing so. 

The technology employed to achieve their approach involves Git and Github. The articles are commissioned by editors. Publications are required to work their way through a staging process. 

The PH team own a Github account, making use of the commenting and issue tracking capabilities to manage the amount of articles. The community also use Github to engage with the authors and discuss possible revisions. Second, the revisions being suggested are enforced by the review guidelines. See https://programminghistorian.org/en/reviewer-guidelines. The articles then evolve by being exposed to continuous iteration. When editors are happy with the results the issue tracking is set to close and the server begins generating a copy for the main website.  

Additionally, if code examples are present, it is a requirement to have the code run by other people during review. 

# A case for Debian Academy  
The Debian project was founded on the policy of being free and open. In keeping with the original principles of Debian, I think there is a strong case for offering transparency in our effort to create a learning platform that chimes with Debian.  

Further, developing an open review process will void the Debian Academy of discrimination, allowing all walks of life to get involved. We must consider that sometimes people believe that writing technical mat-
erial is only reserved for veteran programmers and academics. We have an opportunity to squash these preconceptions by taking contributions from everyone regardless.  

The pay-off for inclusively may lead to simple tutorials which will then become in-demand. For instance, verbose scholarly writing will be caught during reviews. Why write "execute this command" when "run" is more elegant? I often think about programming languages' documentation when this topic comes up. I have first-hand experience of people avoiding the official Java docs because of the way the Orcale choose to present text - concepts hidden behind complex descriptions. I have also met people who praised Django and Python docs because they were "easier to interpret".  

So far I have not included any research regarding the benefits of a peer reviewed system over self-publication. Instead I have tried to draw conclusions from the academic communities. Proposing an approach for writing courses is something that I think will need to be discussed within the Debian Academy and investigated further. 

I hope my suggestions ignites interest for a peer review approach. 


Idea for what to include from Paul Sutton (zleap)

When designing applications we need to make sure that apps fit on the screen,  one of my computers is a 10" netbook some of the thunderbird dialogues don't fit on the screen, so can't get to ok button.   I know gnome has libhandy but this is someting that is starting to be sorted out via developers.
