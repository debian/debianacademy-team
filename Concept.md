**A Concept for the Debian Academy**

# 1. PERSONS

~~~
Who is involved, as providers (organizers, trainers) and participants?

What do we know about them, their explicit or guessed needs and
expectations, their prior and/or required knowledge, skills, attitudes?
~~~

## 1.a Providers:

- In general:
  
  "People committed to Debian and Free Software, and with interest to spread
  knowledge about the Debian Project in an audiovisual way."

- Planning:
  
  "These people support the team by figuring out what classes are to be
  created, outlining the topics are covered and finding people to bring the
  classes into existance."

  * Some interested people (Jathan, Mitja, eamanu)
  * Needed and hopefully provided: skills in project organization

- Technical:

  "These people support the infrastructure of the Debian Academy Team
  Resources."

  * People: Teckids and Dominik George
  * plus interested helpers (Jathan, eamanu, mitja)
  * Skills needed and provided: experience in running e-learning tools
    and providing (e-)learning + Debian background

- Instructors:

  "These people create learning material and train learners, they maintain
  all course work in Moodle like enroll students, grading quizes/tests and
  ensuring class scheduling and course completion. They are
  teachers/trainers."

  * Some interested people (samueloph, highvoltage, eamanu); more needed later.
  * Skills needed and provided: experienced Debian contributors, some
    e-teaching experience (I guess?)
  * Wanted: people with experience in both Debian contributions and
    (e-)teaching skills / experience in adult education, after we have
    defined the aim, topics and objectives.

- Designers

  "These people can create various media like: Moodle badges, web artwork,
  tables and charts."

- Other (O)

  "Subject matter experts for various tools latex, H5p content, articles,
  quizes, workbooks, howtos, and audio/video tutorials."


## 1.b Participants/learners/target group:

Debian users who are interested to learn how Debian is made, and find out
how they could help in different areas inside the Debian Project.

-----

***Working on a concept for the Debian Academy following the PATOMCIE model
is currently stalled, cf. <https://lists.debian.org/debian-academy/2020/11/msg00022.html>***
